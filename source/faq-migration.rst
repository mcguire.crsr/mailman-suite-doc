========================
FAQ (2.1 to 3 Migration)
========================

Frequently asked questions about migration from Mailmn 2.1 to 3 and related to
their feature sets post migration.

.. contents::
   :depth: 4
   :local:


1. List password and Moderator passwords are not working
--------------------------------------------------------

Mailman 3 has a completely new account management system, none of the passwords
from Mailman 2 will work with Mailman 3. Each user needs to sign up in
Postorius for a new account to manage their memberships on *all lists* on a
single instance.

Administrators and moderators also can sign up with their email accounts to
manage their lists without having to share a common moderator or admin password
like Mailman 2.1.
