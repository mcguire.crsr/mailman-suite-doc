.. _virtualenv-install:

=======================
Virtualenv Installation
=======================

This is a step-by-step installation guide for Mailman Suite, also sometimes
referred as Mailman 3 Suite or just Mailman 3. There are more than one ways to
have Mailman 3 running on your machine.

The commands in this guide are tailored for Ubuntu/Debian systems.


Dependencies
------------

Python3.7+.
    While Mailman supports any version of Python > 3.6, versions 3.7+ is 
    recommended.

MTA Setup
    Mailman 3 in theory would support any MTA or mail server which can send
    emails to Mailman over LMTP. Officially, there are configurations for
    Postfix_, Exim4_, qmail_ and sendmail_. Mailman Core has a fairly elaborate
    documentation on `setting up your MTA`_. Look below at :ref:`setup-install-core` 
    to find out the location of configuration file ``mailman.cfg`` which
    is mentioned in the documentation above.

    The Web Front-end is based on a Python web framework called Django_. For
    email verification and sending out error logs, Django also must be
    configured to send out emails.

    This guide uses setup instruction for Postfix_.

Sass compiler
    A `sass compiler`_. Syntactically Awesome Stylesheets or Sass is an
    extension of CSS, which is used to style webpages, that adds more power to
    the old CSS syntax. It allows using variables, nested rules, inline imports
    etc. which CSS originally doesn't support. Hyperkitty uses this to generate
    CSS styles.

    You can use the `C/C++ implementation <http://sass-lang.com/libsass>`_. Please
    look at the `installation guide for sass`_ to see how you can get one.


Python development packages
    Python3 dev package. This is required for building Postorius


Fulltext search
    A full text search engine like `Whoosh`_ or `Xapian`_. Whoosh is the easiest
    to setup and can be installed in the virutualenv.

    .. seealso:: :ref:`setup-virtualenv` on how to setup the virtualenv.

Lynx
    An HTML to plaintext converter like ``lynx`` is required by Mailman Core if you
    have configured it to convert emails to plaintext.


Installing Dependencies
-----------------------

To install Mailman Core, you need to following system packages::

    $ sudo apt install python3-dev python3-venv sassc lynx

.. _setup-db:

Setup database
--------------

This guide is based off on Postgresql database engine. Mailman also supports
MySQL/MariaDB in case you have that already running for other applications.::


    $ sudo apt install postgresql

    $ sudo -u postgres psql
    psql (12.5 (Ubuntu 12.5-0ubuntu0.20.04.1))
    Type "help" for help.
    postgres=# create database mailman;
    CREATE DATABASE
    postgres=# create user mailman with encrypted password 'MYPASSWORD';
    CREATE ROLE
    postgres=# grant all privileges on database mailman to mailman;
    GRANT
    postgres=# \q

.. note:: Replace 'MYPASSWORD' with a secret password.

.. _setup-user:

Setup Mailman user
------------------

Create a new user to run all the Mailman services::

    $ sudo useradd -m -d /opt/mailman -s /usr/bin/bash mailman 
    $ sudo chown mailman:mailman /opt/mailman
    $ sudo su mailman

.. _setup-virtualenv:

Virtualenv setup
----------------

Virtualenv is Python's mechanism to create isoated runtime environments.

.. hint:: If you are not familiar with virtualenv, checkout the `user guide for virtualenv`_.


.. note:: Make sure that you are running commands as ``mailman`` user from here forth. :ref:`setup-user`.

Create the virtualenv for Mailman:
::
   $ cd /opt/mailman
   $ python3 -m venv venv

.. _virtualenv-activate:

Activate  virtualenv:
~~~~~~~~~~~~~~~~~~~~~

Activate the created virtualenv:
::
   $ source /opt/mailman/venv/bin/activate

.. note:: The rest of this documentation assumes that
          :ref:`virtualenv is activated <virtualenv-activate>`. Whether or not
          virtualenv is activated can be seen by a ``(venv)`` before the shell
          prompt.


You can setup ``mailman`` user's shell to automatically activate the virtualenv
when you switch to the user by running::


    $ echo 'source /opt/mailman/venv/bin/activate' > /opt/mailman/.bashrc


.. _setup-install-core:

Installing Mailman Core
-----------------------

Mailman Core is responsible for sending and receiving emails. It exposes a REST
API that different clients can use to interact with over an HTTP protocol. The
API itself is an administrative API and it is recommended that you don't expose
it to outside of your host or trusted network. To install Core run::

  (venv)$ pip install wheel mailman psycopg2-binary

This will install `latest release of Mailman Core <https://pypi.org/project/mailman/>`_,
and Python bindings for Postgresql datbase. After this, create a configuration
file at ``/etc/mailman.cfg`` for Mailman Core::

    # /etc/mailman.cfg
    [paths.here]
    var_dir: /opt/mailman/mm/var

    [mailman]
    layout: here
    # This address is the "site owner" address.  Certain messages which must be
    # delivered to a human, but which can't be delivered to a list owner (e.g. a
    # bounce from a list owner), will be sent to this address.  It should point to
    # a human.
    site_owner: user@example.com

    [database]
    class: mailman.database.postgresql.PostgreSQLDatabase
    url: postgres://mailman:MYPASSWORD@localhost/mailman

    [archiver.prototype]
    enable: yes

    [shell]
    history_file: $var_dir/history.py

    [mta]
    verp_confirmations: yes
    verp_personalized_deliveries: yes
    verp_delivery_interval: 1

.. seealso:: The further configuration setup for Mailman Core
             :ref:`config-core`.

.. _setup-postfix:

Setup MTA
---------
A Mail Transfer Agent (MTA) is responsible for sending and receiving Emails on
the server. This guide is based off on Postfix MTA, but Mailman also supports
`other MTAs like Exim4 etc 
<https://docs.mailman3.org/projects/mailman/en/latest/src/mailman/docs/mta.html#postfix>`_::

    $ sudo apt install postfix

Choose "Internet Site" when prompted during installation for choosing
Postfix configuration.

.. image:: ../../_static/postfix-install-config.jpg
   :width: 400px
   :alt: Postfix install configuration

Enter the domain name you have chosen for Mailman in next step.

.. image:: ../../_static/postfix-choose-mailname.jpg
   :width: 400px
   :alt: Postfix choose server mail name


To configure Postfix to relay emails to and from Mailman add the following to
Postfix's configuration at ``/etc/postfix/main.cf``::

    unknown_local_recipient_reject_code = 550
    owner_request_special = no

    transport_maps =
        hash:/opt/mailman/mm/var/data/postfix_lmtp
    local_recipient_maps =
        hash:/opt/mailman/mm/var/data/postfix_lmtp
    relay_domains =
        hash:/opt/mailman/mm/var/data/postfix_domains

.. seealso:: See detailed documentation to 
             `setup Postfix with Core 
             <https://docs.mailman3.org/projects/mailman/en/latest/src/mailman/docs/mta.html#postfix>`_
             and some unusual configuration if you already have Postfix running.

Starting Mailman automatically
------------------------------

To start Mailman Core automatically on boot, you can setup a systemd service. Create a 
new file ``/etc/systemd/system/mailman3.service``::


    [Unit]
    Description=GNU Mailing List Manager
    After=syslog.target network.target postgresql.service

    [Service]
    Type=forking
    PIDFile=/opt/mailman/mm/var/master.pid
    User=mailman
    Group=mailman
    ExecStart=/opt/mailman3/venv/bin/mailman start
    ExecReload=/opt/mailman3/venv/bin/mailman restart
    ExecStop=/opt/mailman3/venv/bin/mailman stop

    [Install]
    WantedBy=multi-user.target

You can load this configuration by running::

    $ sudo systemctl daemon-reload
    # Check the status of the service.
    $ sudo systemctl status mailman3

To start the systemd service and Mailman Core::

    $ sudo systemctl start mailman3
    # Verify that the service is running.
    $ sudo systemctl status mailman3

After this, running ``mailman info`` (as ``mailman`` user with 
:ref:`virtualenv active <virtualenv-activate>` ) should give you an 
output which looks something like below::

  (venv)$ mailman info
  GNU Mailman 3.3.2 (Tom Sawyer)
  Python 3.8.5 (default, Jul 28 2020, 12:59:40) 
  [GCC 9.3.0]
  config file: /etc/mailman.cfg
  db url: postgres://mailman:MYPASSWORD@localhost/mailman
  devmode: DISABLED
  REST root url: http://localhost:8001/3.1/
  REST credentials: restadmin:restpass


.. warning:: Pay attention to the ``config file`` output above and make
  sure that you see ``/etc/mailman.cfg`` there. If the config path is different,
  you should make sure to create a new config file at ``/etc/mailman.cfg`` and
  re-run the command.  


.. _setup-install-web:

Installing Web UI
-----------------

Postorius and Hyperkitty are Mailman's official Web UI and Archiver. Both of
them are Django based apps and can be integrated into an existing Django
installation/website.


Installing Postorius & Hyperkitty
=================================

Postorius and Hyperkitty are Django "apps" and can be directly installed using
the commands below::

  (venv)$ pip install postorius
  (venv)$ pip install hyperkitty


mailman-hyperkitty plugin enables interaction between Core and Hyperkitty. You
can install it using::

  (venv)$ pip install mailman-hyperkitty

Note that mailman-hyperkitty is a plugin for Mailman Core and not Django.


.. _setting-up-django-project:

Setting up Django Project
=========================

.. seealso:: :ref:`What is Django? <django-knowledge>`


If you have absolutely no idea about Django, just clone/download this
`mailman-suite repo`_ . It should have a documented ``settings.py`` and a
pre-configured ``urls.py`` which you can use to run new Django installation. If
you find any setting that is not documented, please look at `Django's settings
reference`_ for all available settings.

Exact commands would look something like this::

  # Download and install the latest release of django.
  (venv)$ pip install Django>=2.2

  # Clone the repo locally.
  (venv)$ git clone https://gitlab.com/mailman/mailman-suite.git
  (venv)$ cd mailman-suite/mailman-suite_project/

  # Create the tables in the database and load fixtures.
  (venv)$ python3 manage.py migrate

  # Copy all the static files to one single location.
  (venv)$ python3 manage.py collectstatic

  # Run the Django's "development" server at localhost:8000
  (venv)$ python3 manage.py runserver


.. _setup-wsgi:

Setting up a WSGI server
------------------------

.. seealso:: :ref:`What is WSGI? <about-wsgi>`

These instructions are to setup your Django website behind a webserver. We are
using a `uwsgi <http://uwsgi-docs.readthedocs.io/>`_ as the wsgi server to
communicate between the webserver and Django. To install uwsgi, run::

  (venv)$ pip install uwsgi

.. note:: The configuration below doesn't serve static files, so if you are just
          "trying-it-out" and want static files to be served, you need to add
          some additional configuration and steps. See `serving static files
          with uwsgi`_.

.. seealso:: :ref:`Why does my django site look ugly? <django-static-files>`

.. seealso:: `django uwsgi docs`_

Then you can configure it using the following configuration file::


  # uwsgi.ini
  #
  [uwsgi]
  # Port on which uwsgi will be listening.
  http-socket = 0.0.0.0:8000

  # Move to the directory wher the django files are.
  chdir = /path-to-django-project-directory/

  # If running uwsgi from the virtual environment ...
  virtualenv = /your/virtual/env

  # Use the wsgi file provided with the django project.
  wsgi-file = wsgi.py

  # Setup default number of processes and threads per process.
  master = true
  process = 2
  threads = 2

  # Drop privielges and don't run as root.
  uid = 1000
  gid = 1000

  # Setup the django_q related worker processes.
  attach-daemon = ./manage.py qcluster
  # If you are running uwsgi from virtual env, use this instead
  # attach-daemon = /your/virtual/env/bin/python3 ./manage.py qcluster

  # Setup the request log.
  req-logger = file:/path-to-logs/logs/uwsgi.log

  # Log cron seperately.
  logger = cron file:/path-to-logs/logs/uwsgi-cron.log
  log-route = cron uwsgi-cron

  # Log qcluster commands seperately.
  logger = qcluster file:/path-to-logs/logs/uwsgi-qcluster.log
  log-route = qcluster uwsgi-daemons

  # Last log and it logs the rest of the stuff.
  logger = file:/path-to-logs/logs/uwsgi-error.log


You can run uwsgi using the following command::

  (venv)$ uwsgi --ini /path/to/uwsgi.ini

Note that in the above configuration, there is a command called ``python
manage.py qcluster`` which run the ``django-q`` processes. You can remove this
from here if you want to manage this yourself via some other init process.

Have a look at `uwsgi`_ documentation to learn more about different
configuration options. One minor optimization that can be done is to replace::

  http-socket = 0.0.0.0:8000

with a more performant option::

  uwsgi-socket = 0.0.0.0:8000

However, this requires support for uwsgi protocol in the webserver. Nginx and
Apache, the two most popular web-servers have the support and the Nginx
configuration below uses ``uwsgi-socket``.


Nginx Configuration
-------------------

You can reverse proxy the requests to uwsgi server using Nginx. Uwsgi has a
special protocol called uwsgi protocol that is available in Nginx as a
plugin. Add the following configuration to your sites-availbale in Nginx


.. note:: This configuration relies on uwsgi listening on a ``uwsgi-socket``
          instead of a ``http-socket``. Please make sure before using this
          configuration.

          Also, if you must use the ``http-socket`` option for some reason,
          replace the line below with ``uwsgi_pass 0.0.0.0:8000;`` with
          ``proxy_pass http://0.0.0.0:8000;``.

::

     server {

        listen 443 ssl default_server;
        listen [::]:443 ssl default_server;

        server_name MY_SERVER_NAME;
        location /static/ {
             alias /path-to-djang-staticdir/static/;
        }
        ssl_certificate /path-to-ssl-certs/cert.pem;
        ssl_certificate_key /path-to-ssl-certs/privkey.pem;

        location / {
                include uwsgi_params;
                uwsgi_pass 0.0.0.0:8000;

        }

     }

Fill in the appropriate paths above in the configuration before using it.

.. _setting up your MTA: https://mailman.readthedocs.io/en/latest/src/mailman/docs/mta.html
.. _uwsgi : https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html
.. _django uwsgi docs: https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/uwsgi/
.. _setup email backend for django: https://docs.djangoproject.com/en/1.11/topics/email/#smtp-backend
.. _sass compiler : http://sass-lang.com/
.. _installation guide for sass : http://sass-lang.com/install
.. _Whoosh : https://pythonhosted.org/Whoosh/
.. _Xapian : https://xapian.org/
.. _configure Mailman Core: https://mailman.readthedocs.io/en/latest/src/mailman/config/docs/config.html
.. _database of Mailman Core : https://mailman.readthedocs.io/en/latest/src/mailman/docs/database.html
.. _Django : https://www.djangoproject.com/
.. _WSGI : https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface
.. _mailman-suite repo : https://gitlab.com/mailman/mailman-suite/tree/master
.. _Django's settings reference: https://docs.djangoproject.com/en/1.11/ref/settings/
.. _database for mailman core: https://mailman.readthedocs.io/en/latest/src/mailman/docs/database.html
.. _`serving static files with uwsgi`: https://uwsgi-docs.readthedocs.io/en/latest/StaticFiles.html
.. _Postfix: http://www.postfix.org/
.. _Exim4: http://www.exim.org/
.. _sendmail: https://www.proofpoint.com/us/sendmail-open-source
.. _qmail: http://cr.yp.to/qmail.html
.. _django-haystack : https://django-haystack.readthedocs.io/en/master/backend_support.html
.. _user guide for virtualenv: https://virtualenv.pypa.io/en/stable/user_guide.html
